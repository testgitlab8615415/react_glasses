import React from 'react'
import ProductItem from './ProductItem'
const ProductList = ({HandleProductDetail,data,HandleSrcImage}) => {
  return (<div className="container mt-4 pb-4"style={{backgroundColor:"pink", borderRadius:"10px"}}>
    <div className='row'>
      {data.map((item)=>{
         return <ProductItem HandleProductDetail={HandleProductDetail} HandleSrcImage={HandleSrcImage} item={item}  key={item.id} />
      })

      }
    </div></div>
  )
}

export default ProductList
