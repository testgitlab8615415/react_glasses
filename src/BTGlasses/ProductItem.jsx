import React from 'react'

const ProductItem = (props) => {
  const {item, HandleSrcImage}=props;

  return (
    <div className='col-2 pt-5' style={{position:"relative",zIndex:10000}}>

      <div className="card" >
        <button className='btn btn-outline-warning' style={{width:"100%",height:"100%",}}>
          <img style={{width:100 , height:50}}  onClick={()=>HandleSrcImage(item)} src={item.url}  alt="..." /></button>
     
      </div>
      
    </div>
  )
}

export default ProductItem
