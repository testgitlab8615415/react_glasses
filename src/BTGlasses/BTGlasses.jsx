import React, { useState } from 'react'
import data from './data.json'
import ProductList from './ProductList';
const BTGlasses = () => {
    const [srcImgae,setSrcImgage]=useState('./glassesImage/v1.png')
    const [PrdDetail,setProductDetail] =useState(data[0])

    const HandleSrcImage=(product)=>{
     setSrcImgage(product.url);
     setProductDetail(product);
     
    }
  return (
    <div style={{backgroundImage: "url('./glassesImage/background.jpg')",backgroundRepeat:"no-repeat",height:"100vh",backgroundSize:"cover" }}>
    <div className='container'>
      
        <h1 className='text-center' style={{color: "rgb(107 133 133 )",height:70, backgroundColor:"rgb(126 126 126 / 30%)"}}>Bài tập Glassess</h1>
        <div className="img d-flex">
      <div className="image1" style={{ position:"relative",zIndex:10,top:155,left:110}}>
        <img style={{width:"65%"}}  src={srcImgae} alt="..." />
      <div style={{position:"absolute",width:"120%",bottom:155, left:-110}} className="card-footer">
       <h2 style={{color:"#ffc107"}}>{PrdDetail.name}</h2>
       <p style={{fontSize:18}}>{PrdDetail.desc}</p>
       <p style={{fontSize:30}}className='font-weight-bold'>{PrdDetail.price}</p>
      </div>
      </div>
      <div className="imgage3" style={{position:"absolute"}}>
      <img  src="./glassesImage/model.jpg" alt="" />
      </div>
      <div className="image2" style={{marginLeft:220}}> 
             <img  src="./glassesImage/model.jpg" alt="" />
</div></div>

      <ProductList HandleSrcImage={HandleSrcImage} data={data}/>

    </div></div>
  )
}

export default BTGlasses
